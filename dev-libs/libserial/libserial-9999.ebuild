# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
inherit cmake-utils git-2

EGIT_REPO_URI="https://github.com/crayzeewulf/libserial.git"
SRC_URI=""

HOMEPAGE="https://sourceforge.net/projects/libserial/"
DESCRIPTION="An iostream wrapper for serial ports."

KEYWORDS="~x86 ~amd64"
LICENSE="GPL-2"
SLOT="0"

RDEPEND="dev-python/sip app-doc/doxygen"
DEPEND="${RDEPEND}"

#src_unpack() {
#    subversion_src_unpack
#    eautoreconf || die "eautoconf failed"
#}

src_configure() {
	cmake-utils_src_configure
}

src_compile(){
	cd ${BUILD_DIR}
	emake
}

src_install() {
	cd ${BUILD_DIR}
	emake DESTDIR=${D} install
}
