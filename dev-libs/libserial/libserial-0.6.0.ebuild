# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
inherit autotools

SRC_URI="http://archive.ubuntu.com/ubuntu/pool/universe/libs/libserial/libserial_0.6.0~rc2+svn122.orig.tar.gz"

HOMEPAGE="https://sourceforge.net/projects/libserial/"
DESCRIPTION="An iostream wrapper for serial ports."

KEYWORDS="~x86 ~amd64"
LICENSE="GPL-2"
SLOT="0"

RDEPEND="dev-python/sip app-doc/doxygen"
DEPEND="${RDEPEND}"

src_unpack() {
	if [ "${A}" != "" ]; then
		unpack ${A}
		mv libserial-0.6.0~rc2+svn122 libserial-0.6.0
	fi
}

src_prepare() {
	default
	eautoreconf
}

src_configure() {
	if [[ -x ${ECONF_SOURCE:-.}/configure ]] ; then
		econf
	fi
}

src_compile(){
	pwd
	echo "all:" > sip/Makefile
	echo "install:" >> sip/Makefile
	emake
}

src_install() {
#    cd ${BUILD_DIR}
	emake DESTDIR=${D} install
}
